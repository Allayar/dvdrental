WITH ActorActivity AS (
    SELECT
        actor.actor_id,
        actor.first_name,
        actor.last_name,
        MIN(film.release_year) AS first_movie_year,
        MAX(film.release_year) AS last_movie_year
    FROM
        public.actor AS actor
    JOIN public.film_actor AS film_actor ON actor.actor_id = film_actor.actor_id
    JOIN public.film AS film ON film_actor.film_id = film.film_id
    GROUP BY
        actor.actor_id, actor.first_name, actor.last_name
)
SELECT
    aa.first_name,
    aa.last_name,
    (SELECT EXTRACT(YEAR FROM CURRENT_DATE) - MAX(aa.last_movie_year)) AS years_inactive
FROM
    ActorActivity AS aa
GROUP BY
    aa.first_name, aa.last_name
ORDER BY
    years_inactive DESC;
