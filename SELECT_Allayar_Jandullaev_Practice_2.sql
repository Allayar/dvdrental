WITH TopRentedMovies AS (
    SELECT
        f.film_id,
        f.title AS t,
        COUNT(*) AS rental_count
    FROM
        public.rental AS r
    JOIN public.inventory AS i ON r.inventory_id = i.inventory_id
    JOIN public.film AS f ON i.film_id = f.film_id
    GROUP BY
        f.film_id, f.title
    ORDER BY
        rental_count DESC
    LIMIT 5
)
SELECT
    trm.t AS movie_title,
    AVG(EXTRACT(YEAR FROM current_date) - EXTRACT(YEAR FROM c.create_date)) AS expected_age,
    trm.rental_count
FROM
    TopRentedMovies AS trm
JOIN public.inventory AS i ON trm.film_id = i.film_id
JOIN public.rental AS r ON i.inventory_id = r.inventory_id
JOIN public.customer AS c ON r.customer_id = c.customer_id
GROUP BY
    trm.t, trm.rental_count
ORDER BY
    trm.rental_count DESC;
