WITH revenue_store AS
(
  SELECT 
    s.store_id,
    a.address AS store_address, 
    staff.staff_id,
    staff.first_name,
    staff.last_name,
    SUM(p.amount) AS revenue
    FROM 
       payment AS p
    JOIN rental AS r ON r.rental_id = p.rental_id
    JOIN inventory AS i ON i.inventory_id = r.inventory_id
    JOIN store AS s ON s.store_id = i.store_id
    JOIN address AS a ON a.address_id = s.address_id
    JOIN staff ON staff.store_id = s.store_id
    WHERE 
       p.payment_date BETWEEN '2017-01-01'::date AND '2017-12-31'::date
    GROUP BY 
       s.store_id, a.address, staff.staff_id, staff.first_name, staff.last_name
)

SELECT store_id, store_address, staff_id, first_name, last_name, revenue
FROM (
  SELECT
    store_id,
    store_address, -- Include store_address in the subquery
    staff_id,
    first_name,
    last_name,
    revenue,
    ROW_NUMBER() OVER (PARTITION BY store_id ORDER BY revenue DESC) AS row_num
  FROM revenue_store
) ranked
WHERE row_num = 1;
